import os,psutil,json,datetime

class Rasp:	
	def __init__(self):
	    self.data=[]
	
	def get_cpu_temperature(self):
	    with open('/sys/class/thermal/thermal_zone0/temp', 'r') as f:
	        temp = int(float(f.read()) / 1000.0)
	    return temp
	
	def getRam(self):
	    arr={'Shared':psutil.virtual_memory().shared>>20,
	    	'Cached':psutil.virtual_memory().cached>>20,
	    	'Buffers':psutil.virtual_memory().buffers>>20,
	    	'Inactive':psutil.virtual_memory().inactive>>20,
	    	'Active':psutil.virtual_memory().active>>20,
	    	'Free':psutil.virtual_memory().free>>20,
	    	'Used':psutil.virtual_memory().used>>20,
	    	'Percent':psutil.virtual_memory().percent,
	    	'Available':psutil.virtual_memory().available>>20,
	    	'Total':psutil.virtual_memory().total>>20}
	    return arr

        def getDiskUsage(self):
            arr={'Total':psutil.disk_usage('/').total>>20,
                    'Used':psutil.disk_usage('/').used>>20,
                    'Free':psutil.disk_usage('/').free>>20,
                    'Percent':psutil.disk_usage('/').percent}
            return arr
        
        def getDiskPartition(self):
            return psutil.disk_partitions()

        def getBootTime(self):
            return datetime.datetime.fromtimestamp(psutil.boot_time()).strftime("%Y-%m-%d %H:%M:%S")

        def getUsers(self):
            return psutil.users()

        def getNetworkInfo(self):
            return psutil.net_io_counters(pernic=True)

        def getNetworkConnection(self):
            return psutil.net_connections()

        def getNetworkInterface(self):
            return psutil.net_if_addrs()
	
	def getAllPid(self):
	    array = []
	    print(psutil.process_iter())
	    for proc in psutil.process_iter():
	        try:
	            pinfo = proc.as_dict(attrs=['pid', 'name','cpu_percent','ppid','terminal','status','num_threads'])
	        except psutil.NoSuchProcess:
	            pass
	        else:
	            array.append(pinfo)
	    return array

